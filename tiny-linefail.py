#!/usr/bin/python

import sys

import pygame

#from pygame.locals import *



SCREEN_SIZE = (1280, 960)
screen = None



def init():
    global screen
    
    pygame.init()
    screen = pygame.display.set_mode(SCREEN_SIZE)


def main():
    print("Running main...")

    # prepare the display
    screen.fill((255,255,255))
    pygame.display.flip()
       
    # draw us a line
    pygame.draw.line(screen, (0,0,0), (100, 100), (100, 500))

    #draw fancy lines
##    bands = ( (255, 0, 0), (0, 255, 0), (0, 0, 255) )
    bands = ( (0, 255, 255), (255, 0, 255), (255, 255, 0) )
    
    x = 100
  
    color_band = 0

    screen.lock()

    # single colors
    x += 50
    for y in range(100, 500):
        screen.set_at((x, y), bands[0])
        screen.set_at((x + 10, y), bands[1])
        screen.set_at((x + 20, y), bands[2])

    x += 20

    # draw a peturbed line:
    x += 50
    for y in range(100, 500):
        
        screen.set_at((x, y), bands[color_band])
        color_band += 1
        if color_band >= len(bands):
            color_band = 0

    # draw a zigzag
    zag = False
    x += 50
    
    for y in range(100, 500):
        
        screen.set_at((x, y), bands[color_band])

        if not zag:
            color_band += 1
            if color_band >= len(bands):
                zag = True
                color_band = len(bands) -1
        else:
            color_band -= 1
            if color_band < 0:
                zag = False
                color_band = 0
            
    screen.unlock()

    pygame.display.flip()


    game_running = True
    while game_running:

        # draw the display

        # event handler
        for event in pygame.event.get():
            event_handled = False
            
            if event.type == pygame.QUIT:
                game_running = False
                event_handled = True
                
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    game_running = False
                    event_handled = True

            elif event.type == pygame.MOUSEMOTION:
                # don't spam mouse motion events
                event_handled = True

            if not event_handled:
                sys.stdout.write("Unhandled event: ")
                print(event)




if __name__ == "__main__":
    init()

    try:
        main()
    except:
        pygame.quit()
        raise
    
    # we are done...
    pygame.quit()


