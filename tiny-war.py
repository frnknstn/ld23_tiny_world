#!/usr/bin/python

from __future__ import division

import sys
import os
import random

import time

import pygame

from defines import *
import interface
import mobiles

# make a 2D array of bools for the grid
terrain = [ [None] * CELLS[1] for x in range(CELLS[0]) ]
objects = [ [None] * CELLS[1] for x in range(CELLS[0]) ]

screen = None
sprites = []
mobs = []

# Exceptions
class TinyException(Exception):
    pass

class GameInitException(TinyException):
    pass

class SpriteException(TinyException):
    pass

# Classes

class Sprite(object):
    def __init__(self, surface = None, filename = None, description = ""):
        if surface is None and filename is None:
            raise SpriteException("Must supply either a surface or filename to load")
        
        if surface is not None:
            self.filename = None
            self.surface = surface
        if filename is not None:
            self.filename = filename
            self.surface = pygame.image.load(filename)

        self.description = description

        self.surface.set_alpha(None)
        self.surface.set_colorkey((0,0,0))
        self.surface.convert()

        
class FramerateCounter(object):
    buffer_size = 12
    
    def __init__(self):
        self.rates = [0 for i in range(self.buffer_size)]
        self.average = 0.0
        self.last_time = time.clock()
        self.index = 0
        self.zero_interval_frames = 0
        self.font = pygame.font.SysFont("", 16)


    def frame(self):
        """signal the next frame to the FPS counter, and return the current average framerate"""
        this_time = time.clock()
        this_interval = (this_time - self.last_time)
        if this_interval == 0:
            # Too short a time between frames, or insufficient timer resolution
            # Don't log this frame
            self.zero_interval_frames += 1
            return self.average

        this_rate = (1 + self.zero_interval_frames) / (this_time - self.last_time)
        self.zero_interval_frames = 0

        # circular buffer for frame rates
        self.index += 1
        if self.index >= self.buffer_size:
            self.index = 0
        self.rates[self.index] = this_rate

        self.last_time = this_time
        self.average = sum(self.rates) / self.buffer_size

        return self.average

    def draw(self):
        global screen
        screen.blit(self.font.render("FPS: %0.3f" % self.average, True, (0xFF, 0xFF, 0xFF), (0,0,0)), (0, 0))



mouse_over_box = None

# Game functions
def init():
    global screen
    global mouseover_box
    global ui
    global fps

    debug("Initializing...")

    # Create the sprite dir if it does not exist
    if not os.path.isdir(SPRITE_DIR):
        error_message = " *** Error: Cannot find required directory '%s'" % SPRITE_DIR
        raise GameInitException(error_message)

    pygame.init()
    screen = pygame.display.set_mode(SCREEN_SIZE)

    ui = interface.Interface(screen)
    mouseover_box = interface.MouseOverBox()

    mobiles.init(screen, terrain, objects)

    fps = FramerateCounter()
    

def load_sprites():
    """Load the sprites in SPRITE_DIR into the UI"""
    global sprites
    
    debug("Loading sprites...", DEBUG_INIT)

    filenames = os.listdir(SPRITE_DIR)
    filenames = [ os.path.join(SPRITE_DIR, x) for x in filenames if x.endswith(".bmp") ]
    filenames.sort()
    debug("Found %d files to load" % len(filenames), DEBUG_INIT)

    # load all the files!
    index = -6
    for filename in filenames:
        debug("Loading file %s" % filename, DEBUG_INIT)
        surf = pygame.image.load(filename)

        # split into 2x4 sprites
        for y in range(2):
            for x in range(3):
                area = ((3 * x, 6 * y), (2,4))
                subsprite = pygame.Surface((2, 4))
                subsprite.blit(surf, (0,0), area)

                if index <= 255 - 65:
                    char_repr = repr(chr(index + 65))
                else:
                    char_repr = '""'
                description = "%d %s, number %d from file %s" % (
                    index, char_repr, x + (y*3) + 1, filename)

                sprites.append(Sprite(subsprite, description = description))
                index += 1


def main():
    print("Running main...")

    # prepare the display
    screen.fill((0,0,0))

    load_sprites()

    for element in interface.interface_list:
        element.draw()


    for i in range(10000):
        pos = (random.randint(0,CELLS[0]-1), random.randint(0,CELLS[1]-1))
        mob = mobiles.Mob(sprites[random.randint(6, 26+6)], pos)
        mobs.append(mob)
        mob.draw()
        

    
    pygame.display.flip()

    frame_count = 0
    mouse_rect = ((0,0), (1,1))    # last position of the mouse cursor

    game_running = True
    while game_running:
        
        frame_count += 1

        fps.frame()
        fps.draw()


        for mob in mobs:
            if mob.pos is None:
                continue
            
            x, y = mob.pos
            x += 1
            if x >= CELLS[0]:
                x = 0
            mob.move_to((x,y))
            mob.draw()

        pygame.display.flip()

        
        pygame.time.wait(1)

        # event handler
        for event in pygame.event.get():
            event_handled = False
            
            if event.type == pygame.QUIT:
                game_running = False
                event_handled = True
                
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    game_running = False
                    event_handled = True

            elif event.type == pygame.MOUSEMOTION:
                mouse_rect = (event.pos, (1,1))
                event_handled = True
                
            elif event.type == pygame.MOUSEBUTTONDOWN:
                event_handled = True                    

            elif event.type == pygame.MOUSEBUTTONUP:
                event_handled = True

            elif event.type == pygame.ACTIVEEVENT:
                # mouseover the window
                event_handled = True

            elif event.type == pygame.VIDEOEXPOSE:
                # A portion of our window is hidden / visible
                event_handled = True

            if not event_handled:
                sys.stdout.write("Unhandled event: ")
                print(event)



        # check the mouseover, for the Mouseover Box
        if pygame.Rect(MAP_RECT).collidepoint(mouse_rect[0]):
            x, y = mouse_rect[0][0] // CELL_SIZE[0], mouse_rect[0][1] // CELL_SIZE[1]
            obj = objects[x][y]
            if obj is not None:
                mouseover_box.update(obj)
                mouseover_box.draw()


if __name__ == "__main__":
    init()

    try:
        main()
    finally:
        pygame.quit()


