import pygame

pygame.init()
screen = pygame.display.set_mode((640,480))

s = pygame.Surface((10,10))
# Workaround for this bug:
#s = pygame.Surface((10,10), pygame.SRCALPHA)

# make the test images
s.fill((0,0,0))
pygame.image.save(s, "test.bmp")
pygame.image.save(s, "test.png")

# load the surfaces
s_bmp = pygame.image.load("test.bmp")
s_png = pygame.image.load("test.png")

s_bmp.convert()
s_png.convert()

# test
red = pygame.Color(255, 0, 0, 255)

s_bmp.set_at((0,0), red)
s_png.set_at((0,0), red)

print("Set color to %s, with red = %d" % (str(red), red.r))
print(".BMP color = %s" % str(s_bmp.get_at((0,0))))
print(".PNG color = %s" % str(s_png.get_at((0,0))))

png_color = s_png.get_at((0,0))
print(".PNG red channel = %d, should be %d" % (png_color.r, red.r))

pygame.quit()


