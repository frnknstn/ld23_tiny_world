#!/usr/bin/python
from __future__ import division

import os

import pygame
import mobiles

from defines import *

# classes
class Interface(object):
    def __init__(self, scr):
        global screen
        screen = scr


class InterfaceItem(object):
    """UI Item"""
    def __init__(self):
        debug("Making a new interface item!")
        self.font = pygame.font.Font(None, 14)
        interface_list.append(self)

class MouseOverBox(InterfaceItem):
    """Interface that shows details of the objects the mouse is over"""
    def __init__(self):
        super(MouseOverBox, self).__init__()

        self.rect = MOUSEOVERBOX_RECT
        self.text = ""

        self.draw()

    def draw(self):
        pygame.draw.rect(screen, (0,0,0), self.rect, 0)
        pygame.draw.rect(screen, (255,255,255), self.rect, 1)
        draw_text(self.text, screen, self.rect)

    def update(self, mob):
        self.text = mob.sprite.description
        
        

# helper functions
def draw_text(text, surface, rect, font_filename = None, size = 14, color = (255,255,255)):
    """Draw a line or paragraph of text, wrapping at a supplied pixel width"""

    if text == "" or text is None:
        return

    # prepare the font
    if font_filename is None:
        font_filename = DEFAULT_FONT
    font_filename = os.path.join(FONT_DIR, font_filename)
    debug("Using font %s" % font_filename, DEBUG_TEXT)

    font = pygame.font.Font(font_filename, size)

    # prepare the text
    rendered_lines = []
    lines = text.split("\n")
    width = rect[1][0]

    debug("Wrapping text to %d pixels" % width, DEBUG_TEXT)

    for line in lines:
        words = line.split(" ")
        line_words = []
        line_text = ""
        
        debug("Parsing line: '%s'" % line, DEBUG_TEXT)
                
        # keep adding words until you hit the line length
        for index in range(len(words)):
            word = words[index]

            debug("Adding word '%s' to line" % word, DEBUG_TEXT)
            
            line_words.append(word)
            line_text = " ".join(line_words)

            line_size = font.size(line_text)[0]
            debug("Line is now %d / %d pixels" % (line_size, width), DEBUG_TEXT)

            if line_size > width:
                # too many words!
                line_text = " ".join(line_words[:-1])
                rendered_lines.append(font.render(line_text, True, color))
                line_words = [word]
                debug("Next line now has word '%s'" % word, DEBUG_TEXT)
                                     
        # make sure all words in this line have been rendered:
        if len(line_words) != 0:
            line_text = " ".join(line_words)
            rendered_lines.append(font.render(line_text, True, color))

    # draw the paragraph!
    x, y = rect[0]
    for line_surface in rendered_lines:
        surface.blit(line_surface, (x, y))
        y += font.get_height()
    
    
        
        

interface_list = []

if __name__ == "__main__":
    print("Error: Please run tinywar.py directly")
