# Tiny World #

Incomplete entry for Ludum Dare 23.

The most usable part of this project is tiny-ed.py, which is a functional editor for sub-pixel bitmapped fonts.

Fonts zip from unknown source, licenced OFL.

# History #

My first LD attempt, IIRC.

* I spent too long making the sub-pixel editor
* Then I spent too long hunting down a bug that turned out to be a Pygame bug (bugtest.py)
* Then I spent too long making a bug report and patch for pygame, only to discover the bug had already been fixed in a months-old commit, only there hadn't been an actual pygame release in ~4 years
* Finally I gave up and spent the rest of my time investigating the most efficient way to do collisions in Pygame (collision-test/)

