#!/usr/bin/python
from __future__ import division

import pygame

from defines import *




# classes
class Mob(object):
    """Base class for all mobile entities"""
    def __init__(self, sprite, position):
        self.sprite = sprite

        self.hp = 100

        self.rect =  pygame.Rect((0,0), self.sprite.surface.get_size())
        self.pos = None
        self.dirty_rect = None
        self.move_to(position)


    def move_to(self, pos):
        """Move this mob to a position on the map"""
        if self.pos is not None:
            old_x, old_y = self.pos
            objects[old_x][old_y] = None
            self.dirty_rect = self.rect

        # don't place us in an occupied tile
        x, y = pos
        if objects[x][y] is None:
            self.pos = pos
            objects[x][y] = self

            x, y = pos[0] * CELL_SIZE[0], pos[1] * CELL_SIZE[1]
            
            self.rect = pygame.Rect((x, y), self.rect.size)
        else:
            self.pos = None


    def draw(self):
        """Draw this mob at it current position"""
        if self.pos is not None:
            screen.blit(self.sprite.surface, self.rect)
        if self.dirty_rect is not None:
            screen.blit(black_box, self.dirty_rect)
            


# functions
def init(_screen, _terrain, _objects):
    global screen
    global terrain
    global objects
    
    global black_box

    # steal some globals from the main .py    
    screen = _screen
    terrain = _terrain
    objects = _objects
    
    
    black_box = pygame.Surface(CELL_SIZE)
    black_box.convert()


if __name__ == "__main__":
    print("Error: Please run tiny-war.py directly")
