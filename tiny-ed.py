#!/usr/bin/python
from __future__ import division

import sys
import os

import pygame

VERSION = "0.1"

SPRITE_DIR = "sprites"

SUBCOLOR = ( (220, 0, 0), (0, 180, 0), (0, 0, 255) )
#SUBCOLOR = ( (200, 0, 0), (0, 110, 0), (0, 0, 255) )
#SUBCOLOR = ( (255, 0, 0), (0, 255, 0), (0, 0, 255) )

SCREEN_SIZE = (600, 500)

CELLS = (30, 10)
CELL_SIZE = (16, 48)

# UI position hints
CELL_RIGHT_EDGE = CELLS[0] * CELL_SIZE[0]

THUMB_RECT = ( (CELL_RIGHT_EDGE + 20, 20), ( (CELLS[0] // 3) + 4, CELLS[1] + 4) )
BIG_THUMB_RECT = ( (CELL_RIGHT_EDGE + 20, 50), (CELLS[0] + 4, CELLS[1] * 3 + 4) )
UGLY_THUMB_RECT = ( (CELL_RIGHT_EDGE + 70, 50), (CELLS[0] + 4, CELLS[1] * 3 + 4) )

THUMB_BOTTOM_EDGE = 50 + CELLS[1] * 3 + 4



# make a 2D array of bools for the grid
grid = [ [False] * CELLS[1] for x in range(CELLS[0]) ]

# make a surface for the actual image
sprite = pygame.Surface((CELLS[0] // 3, CELLS[1]))
sprite.fill((0,0,0))

# an over-sized thumbnail
big_sprite = pygame.Surface((CELLS[0], CELLS[1] * 3))
big_sprite.fill((0,0,0))

# ugly 'what I am actually saving' sprite, just for kicks
ugly_sprite = pygame.Surface((CELLS[0], CELLS[1] * 3))
ugly_sprite.fill((0,0,0))

screen = None

# okay, I am hacking in some stuff to manage multiple files
active_filethumb = None
filethumbs = []

class FileThumb(object):
    """A file thumbnail, for my hacked-on file loading thing"""

    def __init__(self, filename, pos):
        """Load a file from disk and display it as a UI element at 'pos'"""
        self.filename = filename

        print("Loading %s" % filename)
        
        self.sprite = pygame.image.load(self.filename)
        self.sprite.convert_alpha()

        font = pygame.font.Font(None, 12)
        self.label = font.render(os.path.split(filename)[-1], True, (255,255,255), (0,0,0))

        size = self.sprite.get_size()
        self.rect = (pos, (size[0] + 8, size[1] + 8))
        self.active = False

        self.draw()

    def save(self):
        print("Saving file %s" % self.filename)
        pygame.image.save(self.sprite, self.filename)


    def activate(self):
        """Set us up as the active file in the UI"""
        global active_filethumb
        global grid

        print("Making file '%s' active" % self.filename)

        if isinstance(active_filethumb, FileThumb):
            old_active_filethumb = active_filethumb
            old_active_filethumb.deactivate()
            old_active_filethumb.draw()            

        self.active = True
        active_filethumb = self

        grid = self.to_grid()
        grid_redraw()

        for x in range(self.sprite.get_width() * 3):
            for y in range(self.sprite.get_height()):
                draw_on_sprite(x, y)
                update_big_thumb(x, y)
                if x % 3 == 2:
                    update_ugly_thumb(x, y)
        blit_thumbnails()

        self.draw()

    def deactivate(self):
        global active_filethumb

        self.save()
        self.active = False
        if active_filethumb == self:
            active_filethumb = None

    def blit_sprite(self):
        """Blit our sprite, because someone has probably modified it"""
        screen.blit(self.sprite, (self.rect[0][0] + 4, self.rect[0][1] + 4))
    
    def draw(self):
        """Draw our UI element, including our borders"""

        # border
        if self.active:
            border_width = 3
            border_color = (255,100,100)
        else:
            border_width = 1
            border_color = (255,255,255)
        
        pygame.draw.rect(screen, (0, 0, 0), self.rect, 3)
        pygame.draw.rect(screen, border_color, self.rect, border_width)

        # sprite
        self.blit_sprite()

        # label
        screen.blit(self.label, (self.rect[0][0] + 9 + self.sprite.get_width(), self.rect[0][1] + 6))
        
        pygame.display.flip()

    def to_grid(self):
        """Convert our data to a 2D array like is used in the main grid"""
        self.sprite.lock()

        retval = [ [False] * self.sprite.get_height() for x in range(self.sprite.get_width() * 3) ]

        for y in range(self.sprite.get_height()):
            for x in range(self.sprite.get_width()):
                r = g = b = False
                color = self.sprite.get_at((x, y))
                if color.r != 0: r = True
                if color.g != 0: g = True
                if color.b != 0: b = True

                retval[x * 3][y] = r
                retval[x * 3 + 1][y] = g
                retval[x * 3 + 2][y] = b
                
        self.sprite.unlock()
        
        return retval



def init():
    global screen

    # Create the sprite dir if it does not exist
    if not os.path.isdir(SPRITE_DIR):
        try:
            os.mkdir(SPRITE_DIR)
        except:
            print(" *** Error creating directory '%s'" % SPRITE_DIR)
            raise
    
    pygame.init()
    screen = pygame.display.set_mode(SCREEN_SIZE)

def blit_thumbnails():
    """blit the thumbnail images"""
    screen.blit(sprite, (THUMB_RECT[0][0] + 2, THUMB_RECT[0][1] + 2))
    screen.blit(big_sprite, (BIG_THUMB_RECT[0][0] + 2, BIG_THUMB_RECT[0][1] + 2))
    screen.blit(ugly_sprite, (UGLY_THUMB_RECT[0][0] + 2, UGLY_THUMB_RECT[0][1] + 2))

    if isinstance(active_filethumb, FileThumb):
        active_filethumb.blit_sprite()
    
    pygame.display.flip()



def debug_print_grid():
    for y in range(len(grid[0])):
        sys.stdout.write("%0d " % y)
        for x in range(len(grid)):
            if grid[x][y]:
                sys.stdout.write("X")
            else:
                sys.stdout.write(" ")
        print


def grid_click(x, y):
    """The user clicked at grid position (x, y)"""

    # global variable to support draw-while-dragging
    global last_grid_pos
    global draw_value

    last_grid_pos = (x, y)

    draw_value = not grid[x][y]
    grid[x][y] = draw_value

    # draw the cell
    if grid[x][y]:
        color = SUBCOLOR[x % 3]
    else:
        color = (0,0,0)
    
    pygame.draw.rect(screen, color,
                     ( (x * CELL_SIZE[0] + 1, y * CELL_SIZE[1] + 1), (CELL_SIZE[0] - 1, CELL_SIZE[1] - 1)) )

    # update the real sprite and the filethumb copy
    draw_on_sprite(x, y)

    update_big_thumb(x, y)
    update_ugly_thumb(x, y)

    # show the changes
    blit_thumbnails()
    pygame.display.flip()

def draw_on_sprite(x, y):
    """update the main sprite and filethumb copy, based on an update to the grid"""
    pixel_pos = (x // 3, y)
    sprite.lock()

    color = sprite.get_at(pixel_pos)
    new_color = SUBCOLOR[x % 3]

    for channel in range(3):
        if new_color[channel] == 0:
            continue

        if grid[x][y]:
            color[channel] = new_color[channel]
        else:
            color[channel] = 0

    sprite.set_at(pixel_pos, color)
    if isinstance(active_filethumb, FileThumb):
        active_filethumb.sprite.set_at(pixel_pos, color)

    sprite.unlock()

    assert active_filethumb.sprite.get_at(pixel_pos) == sprite.get_at(pixel_pos)


def update_big_thumb(x, y):
    """update the big thumbnail based on grid coords. Doesn't do a flip()."""
    pixel_pos = (x, y * 3)
    
    if grid[x][y]:
        new_color = (255,255,255)
    else:
        new_color = (0,0,0)

    big_sprite.lock()
    big_sprite.set_at((pixel_pos[0], pixel_pos[1]), new_color)
    big_sprite.set_at((pixel_pos[0], pixel_pos[1]+1), new_color)
    big_sprite.set_at((pixel_pos[0], pixel_pos[1]+2), new_color)
    big_sprite.unlock()


def update_ugly_thumb(x, y):
    """update the big thumbnail based on grid coords and sprite color. Doesn't do a flip()."""
    pixel_pos = (x // 3 * 3, y * 3)         # clamp the x coord to a multiple of 3
    color = sprite.get_at((x // 3, y))

    ugly_sprite.lock()
    for i in range(3):
        for j in range(3):
            ugly_sprite.set_at((pixel_pos[0] + j, pixel_pos[1] + i), color)
            assert ugly_sprite.get_at((pixel_pos[0] + j, pixel_pos[1] + i)) == color
    ugly_sprite.unlock()


def grid_drag(x, y):
    """Called when the mouse is dragged over the grid field"""

    # global variable to support draw-while-dragging
    global last_grid_pos
    global draw_mode

    if last_grid_pos == (x, y):
        # we just considered this pixel
        return

    if grid[x][y] == draw_value:
        # we don't need to change this pixel
        return

    grid_click(x, y)

def grid_redraw():
    """Redraw the entire grid"""
    
    for y in range(len(grid[0])):
        for x in range(len(grid)):
            if grid[x][y]:
                color = SUBCOLOR[x % 3]
            else:
                color = (0,0,0)
            pygame.draw.rect(screen, color,
                 ( (x * CELL_SIZE[0] + 1, y * CELL_SIZE[1] + 1),
                   (CELL_SIZE[0] - 1, CELL_SIZE[1] - 1)) )

    pygame.display.flip()

def draw_editor():
    """Redraw the lines of the editor"""

    # main grid
    for i in range(CELLS[0] + 1):
        if i % 3 != 0:
            pygame.draw.line(screen, (150,150,150), (i * CELL_SIZE[0], 0), (i * CELL_SIZE[0], CELLS[1] * CELL_SIZE[1]))
        else:
            pygame.draw.line(screen, (255,255,255), (i * CELL_SIZE[0], 0), (i * CELL_SIZE[0], CELLS[1] * CELL_SIZE[1]))

    for i in range(CELLS[1] + 1):
        pygame.draw.line(screen, (255,255,255), (0, i * CELL_SIZE[1]), (CELLS[0] * CELL_SIZE[0], i * CELL_SIZE[1]))

    # preview rectangles
    pygame.draw.rect(screen, (255,255,255), THUMB_RECT, 1)
    pygame.draw.rect(screen, (255,255,255), BIG_THUMB_RECT, 1)
    pygame.draw.rect(screen, (255,255,255), UGLY_THUMB_RECT, 1)


def load_sprites():
    """Load the sprites in SPRITE_DIR into the UI"""
    global filethumbs
    
    print("Loading sprites...")

    filenames = os.listdir(SPRITE_DIR)
    filenames = [ os.path.join(SPRITE_DIR, x) for x in filenames if x.endswith(".bmp") ]
    filenames.sort()
    print("Found %d files to load" % len(filenames))

    # create new empty files if none exists:
    if len(filenames) == 0:
        tmp_surface = pygame.Surface((10, 10), pygame.SRCALPHA)
        tmp_surface.fill((0,0,0))

        for i in range(38):
            tmp_filename = os.path.join(SPRITE_DIR, "%03d.bmp" %i)
            pygame.image.save(tmp_surface, tmp_filename)
            filenames.append(tmp_filename)

    # load all the files!
    x = CELL_RIGHT_EDGE + 7
    y = THUMB_BOTTOM_EDGE
    for filename in filenames:
        y += 20
        if y >= SCREEN_SIZE[1] - 30:
            y = THUMB_BOTTOM_EDGE + 20
            x += 57
        ft = FileThumb(filename, (x, y))
        filethumbs.append(ft)
        
        
    filethumbs[0].activate()


def main():
    print("Running main...")

    # prepare the display
    screen.fill((0,0,0))

    draw_editor()
    load_sprites()
    
    pygame.display.flip()

    # variables for drag-drawing
    drawing = False

    game_running = True
    while game_running:
        pygame.time.wait(7)

        # event handler
        for event in pygame.event.get():
            event_handled = False
            
            if event.type == pygame.QUIT:
                active_filethumb.save()
                game_running = False
                event_handled = True
                
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    game_running = False
                    event_handled = True

            elif event.type == pygame.MOUSEMOTION:
                if drawing:
                    grid_pos = event.pos[0] // CELL_SIZE[0], event.pos[1] // CELL_SIZE[1]
                    if (grid_pos[0] < CELLS[0]) and (grid_pos[1] < CELLS[1]):
                        grid_drag(event.pos[0] // CELL_SIZE[0], event.pos[1] // CELL_SIZE[1])
                    else:
                        # off the grid
                        pass
                    
                event_handled = True

            elif event.type == pygame.MOUSEBUTTONDOWN:
                grid_pos = event.pos[0] // CELL_SIZE[0], event.pos[1] // CELL_SIZE[1]
                if (grid_pos[0] < CELLS[0]) and (grid_pos[1] < CELLS[1]):
                    drawing = True
                    grid_click(event.pos[0] // CELL_SIZE[0], event.pos[1] // CELL_SIZE[1])
                else:
                    # off the grid, check the filethumbs
                    click_rect = pygame.Rect(event.pos, (1,1))
                    index = click_rect.collidelist(filethumbs)
                    if index != -1:
                        filethumbs[index].activate()
                        
                        
                
                event_handled = True                    

            elif event.type == pygame.MOUSEBUTTONUP:
                drawing = False
                event_handled = True
                    

            if not event_handled:
                sys.stdout.write("Unhandled event: ")
                print(event)




if __name__ == "__main__":
    init()

    try:
        main()
    except:
        pygame.quit()
        raise
    
    # we are done...
    pygame.quit()


