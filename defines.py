#!/usr/bin/python
from __future__ import division

VERSION = "0.2"

DEBUG = True
DEBUG_INIT = False
DEBUG_TEXT = False


SPRITE_DIR = "sprites"
FONT_DIR = "fonts"

DEFAULT_FONT = "MateSC-Regular.ttf"

SUBCOLOR = ( (220, 0, 0), (0, 180, 0), (0, 0, 255) )
#SUBCOLOR = ( (200, 0, 0), (0, 110, 0), (0, 0, 255) )
#SUBCOLOR = ( (255, 0, 0), (0, 255, 0), (0, 0, 255) )

SCREEN_SIZE = (1280, 900)

CELLS = (240, 225)
CELL_SIZE = (4, 4)



# interface location hints

PANEL_LEFT = 960

MAP_RECT = ((0, 0), (PANEL_LEFT, SCREEN_SIZE[1]))

MOUSEOVERBOX_RECT = ((PANEL_LEFT + 20, 20),
                     (SCREEN_SIZE[0] - PANEL_LEFT - 30, 400))


# functions
def debug(message, flag = None):
    if flag is None:
        flag = DEBUG
    if flag:
        print(message)



if __name__ == "__main__":
    print("Error: Please run tinywar.py directly")
