
if False:
    class MyException(BaseException):
        pass

    raise MyException("My hovercraft is full of eels")

"""
    Traceback (most recent call last):
      File "C:/Users/frnknstn/Dropbox/frnknstn/ld23_tiny_world/exception-test.py", line 4, in <module>
        raise MyException("My hovercraft is full of eels")
    MyException: My hovercraft is full of eels
"""


if not False:
    class MyException(BaseException):
        def __init__(self, message, animal):
            self.message = message
            self.animal = animal

        def __str__(self):
            return "Message: '%s' Animal: '%s'" % (self.message, self.animal)

    try:
        raise MyException("My hovercraft is full of animals", "eels")
    except MyException, e:
        print e
        raise

    
